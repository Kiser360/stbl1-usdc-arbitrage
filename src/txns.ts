import { Pool } from "@algofi/amm-v0";
import { assignGroupID, makeAssetTransferTxnWithSuggestedParamsFromObject, signLogicSigTransaction, signLogicSigTransactionObject, waitForConfirmation } from "algosdk";
import { algod, botAccount, Config, oneForOneExLogicSig } from "./config";
import { AssetDecimals, AssetIds, quoteStblBuy, StblBuyQuote } from "./utils";

const InProgress = {
    purchase: false,
    sell: false,
};

export async function purchaseStbl(pool: Pool, usdcPayQty: number) {
    if (InProgress.purchase) {
        console.log('Waiting for a previous purchase to finish');
        return;
    }

    if (usdcPayQty < 1) {
        console.log('Purchase too low: $', usdcPayQty);
        return;
    }

    await doInProgress(async () => {
        const quote = quoteStblBuy(pool, usdcPayQty);
        if (quote.stblPrice >= 1 - Config.minProfitMargin || quote.stblPrice >= 1) {
            console.log(`I will not purchase stbl for more than 1$ (${quote.stblPrice})`);
            return;
        }

        console.log(`[Purchase STBL] Total: $${usdcPayQty} (${quote.stblPrice})`);
        const minReceived = quote.stblReceived * (1 - Config.slippagePercent);

        const txns = await pool.getSwapExactForTxns(
            botAccount.addr,
            AssetIds.USDC,
            Math.floor(quote.usdcCost * AssetDecimals),
            Math.floor(minReceived * AssetDecimals),
            false, true, 9000
        );

        const signedTxns = txns.map(tx => tx.signTxn(botAccount.sk));

        await algod.sendRawTransaction(signedTxns).do();
        await waitForConfirmation(algod, txns[0].txID(), 20);
        console.log('STBL Purchase Success', usdcPayQty);
    }, 'purchase');
}

export async function sellStblOneForOne(qtyStblToSell: number) {
    if (qtyStblToSell < 1) {
        console.log('Sell qty too low: $', qtyStblToSell);
        return;
    }

    await doInProgress(async () => {
        const params = await algod.getTransactionParams().do();
        const xferQty = Math.floor(qtyStblToSell * Config.stblSellErrorMargin * AssetDecimals);
        const sendStblTx = makeAssetTransferTxnWithSuggestedParamsFromObject({
            amount: xferQty,
            assetIndex: AssetIds.STBL1,
            from: botAccount.addr,
            to: Config.oneForOneExAddress,
            suggestedParams: {
                ...params,
                fee: 2000,
                flatFee: true,
            }
        });

        const receiveUsdcTx = makeAssetTransferTxnWithSuggestedParamsFromObject({
            amount: xferQty,
            assetIndex: AssetIds.USDC,
            from: Config.oneForOneExAddress,
            to: botAccount.addr,
            suggestedParams: {
                ...params,
                fee: 0,
                flatFee: true,
            }
        });

        const groupedTxns = assignGroupID([sendStblTx, receiveUsdcTx]);

        const tx1Signed = groupedTxns[0].signTxn(botAccount.sk);
        const { blob: tx2Signed, txID: tx2Id } = signLogicSigTransactionObject(groupedTxns[1], oneForOneExLogicSig);

        await algod.sendRawTransaction([tx1Signed, tx2Signed]).do();
        await waitForConfirmation(algod, tx2Id, 20);
        console.log('STBL Sell Success', xferQty / AssetDecimals);

    }, 'sell');
}

async function doInProgress(action: () => Promise<any>, flag: keyof typeof InProgress) {
    InProgress[flag] = true;

    await action().catch(e => {
        console.error(`${flag} failed:`, e);
    });

    InProgress[flag] = false;
}