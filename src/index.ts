import { Pool, PoolType } from "@algofi/amm-v0";
import { filter } from "rxjs";
import { getAccountState, getAssetBalance } from "./accounts";
import { algofi, botAccount, Config } from "./config";
import { currentRound$ } from "./current-round.service";
import { purchaseStbl, sellStblOneForOne } from "./txns";
import { AssetIds, quoteStblBuy } from "./utils";
import BezierEasing from 'bezier-easing';

const [p1x, p1y, p2x, p2y] = Config.riskCurveParams;
const riskCurve = BezierEasing(p1x, p1y, p2x, p2y);

currentRound$.pipe(
    filter(Boolean)
).subscribe(async cr => {
    console.log('Round:', cr);
    let [nanoPool, botAccountState, exAccountState] = [null, null, null] as any[];
    try {

        [nanoPool, botAccountState, exAccountState] = await Promise.all([
            algofi.getPool(PoolType.NANOSWAP, AssetIds.USDC, AssetIds.STBL1),
            getAccountState(botAccount.addr),
            getAccountState(Config.oneForOneExAddress),
        ]);
    } catch(e) {
        console.error('Round failed', e);
        return;
    }

    const usdcBalance = getAssetBalance(botAccountState, AssetIds.USDC);
    const stblBalance = getAssetBalance(botAccountState, AssetIds.STBL1);

    const lowStblPrice = quoteStblBuy(nanoPool, 1).stblPrice;
    const lowRiskAdjustedMaxStblHolding = riskCurve(getPointOnRiskCurve(lowStblPrice)) * Config.maxStblHolding;

    const lowStblToBuy = Math.max(0, lowRiskAdjustedMaxStblHolding - stblBalance);
    const highStblPrice = quoteStblBuy(nanoPool, lowStblToBuy).stblPrice;
    const highRiskAdjustedMaxStblHolding = riskCurve(getPointOnRiskCurve(highStblPrice)) * Config.maxStblHolding;

    // Finally we'll buy the average amount
    const finalStblToHold = Math.floor((lowRiskAdjustedMaxStblHolding + highRiskAdjustedMaxStblHolding) / 2);
    const stblToBuy = Math.max(0, finalStblToHold - stblBalance);

    // This is async but we will not be blocking execution with await
    purchaseStbl(nanoPool, Math.min(usdcBalance, stblToBuy));


    const availableUsdcLiquidity = getAssetBalance(exAccountState, AssetIds.USDC);
    const availableStblToSell = getAssetBalance(botAccountState, AssetIds.STBL1);

    if (availableUsdcLiquidity && availableStblToSell) {
        sellStblOneForOne(Math.min(availableUsdcLiquidity, availableStblToSell));
    }
});

function getPointOnRiskCurve(stblPrice: number) {
    if (stblPrice >= 1) return 0;

    const maxTravelToMaxProfitPrice = 1 - Config.maxProfitStblPrice;
    const distanceToMaxProfitPrice = stblPrice - Config.maxProfitStblPrice;

    return Math.max(0, 1 - (distanceToMaxProfitPrice / maxTravelToMaxProfitPrice));
}

