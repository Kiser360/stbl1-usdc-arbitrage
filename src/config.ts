import { AlgofiAMMClient, Network } from '@algofi/amm-v0';
import { Algodv2, LogicSigAccount, logicSigFromByte, mnemonicToSecretKey } from 'algosdk';
import * as dotenv from 'dotenv';
import { b64ToUint8 } from './utils';
dotenv.config();

export const Config = {
    // maxStblBuy: 10,
    minProfitMargin: .005, // .5 cents
    slippagePercent: .00025,
    accountMnemonic: process.env.ACCOUNT_MNEMONIC as string,
    oneForOneExAddress: 'WJUXYO3WMOB4F33MFUT34Y6SYMO6OZTXGSS4TPDNXK6ZRUIZXLIUWHR7GQ',

    // algodUrl: 'https://node.algoexplorerapi.io',
    algodUrl: 'https://mainnet-api.algonode.network',
    algodPort: 443,

    stblSellErrorMargin: .85,

    maxProfitStblPrice: .95,
    maxStblHolding: 2000,
    riskCurveParams: [.5, 0, 1, .25],
}

export const algod = new Algodv2('', Config.algodUrl, Config.algodPort);
export const algofi = new AlgofiAMMClient(algod as any, Network.MAINNET);
export const botAccount = mnemonicToSecretKey(Config.accountMnemonic);

const oneForOneExLogicSigRaw = 'BiAFAQAE8NaGD8uUkt4BIzgAgCBNGHKdwXYSlrbBG+no2XuYLo3kJF0lcW8t8uG2N7C0GRJBAAIiQzIEgQISgREQRDEWIhKBEhBEMQEjEoEUEEQxECQSgRYQRDEgMgMSgRcQRDEVMgMSgRgQRDEWIgk4ECQSgRoQRDEWIgk4FDEAEoEbEEQxFiIJOBIxEhKBHRBEMRElEjEWIgk4ESEEEhAxESEEEjEWIgk4ESUSEBGBHxBEIkM=';

export const oneForOneExLogicSig = new LogicSigAccount(b64ToUint8(oneForOneExLogicSigRaw));
