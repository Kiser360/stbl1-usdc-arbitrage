import { BehaviorSubject } from "rxjs";
import { backOff } from "exponential-backoff";

export const currentRound$ = new BehaviorSubject(0);

(async () => {
    const status = await backOff(() => fetch('https://node.algoexplorerapi.io/v2/status').then(res => res.json()), {
        delayFirstAttempt: false,
        startingDelay: 1000,
        numOfAttempts: 5,
        retry(e, attemptNumber) {
            console.log(`Failed to get round [${attemptNumber}] ${e}`);
            return true;
        },
    });

    let currentRound = status['last-round'];
    
    while (true) {
        const waitingForNextRoundStatus = await backOff(() => fetch(`https://node.algoexplorerapi.io/v2/status/wait-for-block-after/${currentRound}`).then(res => res.json()), {
            delayFirstAttempt: false,
            startingDelay: 1000,
            numOfAttempts: 5,
            retry(e, attemptNumber) {
                console.log(`Failed to get round [${attemptNumber}] ${e}`);
                return true;
            },
        });

        currentRound = waitingForNextRoundStatus['last-round'];

        currentRound$.next(currentRound);
    }
})();
