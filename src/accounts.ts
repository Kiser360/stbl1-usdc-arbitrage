import { Config } from "./config";
import { AssetDecimals, AssetIds } from "./utils";

const MIN_BALANCE_PER_APP = 1e5;
const MIN_BALANCE_PER_ASSET = 1e5;
const BASE_MIN_BALANCE = 1e5;

export async function getAccountState(addr: string) {
    const res: LookupAccountResponse = await fetch(`${Config.algodUrl}/v2/accounts/${addr}`).then(res => res.json());

    const apps: any[] = res["apps-local-state"] || [];
    const assets: any[] = res.assets || [];
    return {
        address: res.address,
        algoBalance: res.amount as number,
        apps: new Map(apps.map(ls => [ls.id, ls])),
        assets: new Map(assets.map(assetBalance => [assetBalance["asset-id"], assetBalance])),
        minBalance: apps.length * MIN_BALANCE_PER_APP + assets.length * MIN_BALANCE_PER_ASSET + BASE_MIN_BALANCE,
    };
}

export function getAssetBalance(accountState: AccountState, assetId: AssetIds) {
    return (accountState.assets.get(assetId)?.amount || 0) / AssetDecimals;
}

export interface AccountState {
    address: string,
    algoBalance: number,
    apps: Map<number, AppLocalState>,
    assets: Map<number, AssetBalance>,
    minBalance: number,
}

interface LookupAccountResponse {
    "address": string,
    "amount": number | bigint,
    "amount-without-pending-rewards": number | bigint,
    "assets": AssetBalance[],
    "created-at-round": number,
    "deleted": boolean,
    "pending-rewards": number,
    "reward-base": number,
    "rewards": number,
    "round": number,
    "sig-type": "sig" | string,
    "status": "Offline" | string,
    "apps-local-state"?: AppLocalState[]
}

interface AppLocalState {
    "id": number,
    "deleted": boolean,
    "opted-in-at-round": number,
    "closed-out-at-round": number,
    "schema": {
        "num-uint": number,
        "num-byte-slice": number
    },
    "key-value": {
        "key": string,
        "value": {
            "type": 1 | 0,
            "bytes": string,
            "uint": number
        }
    }[]
}

interface AssetBalance {
    amount: number;
    'asset-id': number;
    deleted: boolean;
    'is-frozen': boolean;
    'opted-in-at-round': number;
}

