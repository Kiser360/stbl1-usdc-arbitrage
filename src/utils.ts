import { Pool } from "@algofi/amm-v0";

export const AssetDecimals = 1e6; // Both USDC and STBL have the same configuration
export enum AssetIds {
    STBL1 = 465865291,
    USDC = 31566704,
};

// const NanoFeePercent = .01; // .01%

// export function findMaxProfitableStblBuy(pool: Pool) {
//     const minProfitablePrice = 1 - (NanoFeePercent / 100) - Config.minProfitMargin;
    
//     return binarySearch(0, Config.maxStblBuy, (usdcPayQty: number) => {
//         const result = quoteStblBuy(pool, usdcPayQty);
        
//         return result.stblPrice <= minProfitablePrice;
//     });
// }

export function quoteStblBuy(pool: Pool, usdcPayQty: number): StblBuyQuote {
    
    const stblForUsdcQuote = pool.getSwapExactForQuote(AssetIds.USDC, Math.floor(usdcPayQty * AssetDecimals));

    const stblReceived = Math.abs(stblForUsdcQuote.asset2Delta / AssetDecimals);

    return {
        stblPrice: usdcPayQty / stblReceived,
        stblReceived: stblReceived,
        usdcCost: usdcPayQty,
    }
}

export interface StblBuyQuote {
    stblPrice: number,
    stblReceived: number,
    usdcCost: number,
}

function binarySearch(start: number, end: number, test: (n: number) => boolean): number {
    if (start >= end) return start;

    const mid = start + Math.floor((end - start) / 2);
    const atStart = test(start);
    const atEnd = test(end);
    const atMid = test(mid);

    return atStart && !atMid ? binarySearch(start, mid -1, test)
    : atMid && !atEnd ? binarySearch(mid + 1, end, test)
    : atStart && atEnd ? end
    : !atStart && !atEnd ? start
    : mid;
}

export const _atob = globalThis.atob || ((src: string) => {
    return Buffer.from(src, 'base64').toString('binary');
})

export const _btoa = globalThis.btoa || ((src: string) => {
    return Buffer.from(src, 'binary').toString('base64');
})


export function b64ToUint8(b64: string): Uint8Array {
    return Uint8Array.from(_atob(b64), c => c.charCodeAt(0));
}

